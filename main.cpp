// /////////////////////////////////////////////////////////////// //
// Network Class example main.                                     //
// (c) 2021, J. Neidhart                                           //
// /////////////////////////////////////////////////////////////// //

#include "networkClass.h"



int main(){
    cl_network net;

    net.initSmallPowerNet();

    net.printAdjacency("net1.csv");
    std::cout<<net.ingoingPowerAtNode(0)<<"\t"<<net.outgoingPowerAtNode(0)<<"\t"<<net.totalPowerProduction()<<
        "\t"<<net.numberOfEdges()<<"\t"<<net.totalPowerTransport()<<"\t"<<net.fitness()<<std::endl;
    /*while(net.numberI > 0){
      net.printState();
      net.timestep();
      }
      net.printState();
      net.calculateDegrees();
      net.calculatePathStatistics();
      std::cout<<"Mean Degree: "<<net.meanDegree<<std::endl;

*/
    return(0);
}
